import socket

port = 5000
host = '127.0.0.1'
fname = 'rbs_received.py'

s = socket.socket()
s.connect((host, port))

with open(fname,'wb') as f:
    print ('Receiving file...')
    while True:
        data = s.recv(1024)
        if not data:
            break
        f.write(data)

f.close()

print('Transfer finished.')
s.close()
