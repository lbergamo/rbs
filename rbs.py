import os
import socket
import select
import sys
import pickle

class Ship():
    def __init__(self, typ, ini_pos, orient):
        self.typ = typ
        self.pos = []
        self.pos_hit = []
        for i in xrange(typ):
            if orient == 'h':
                self.pos.append(add_array(ini_pos, [0,i]))
            else:
                self.pos.append(add_array(ini_pos, [i,0]))
        self.life = len(self.pos)
        
    def shoot(self, pos):
        if pos in self.pos:
            hit = 1
            self.life -= 1
            self.pos.remove(pos)
            self.pos_hit.append(pos)
        else:
            hit = 0
        return hit
        
        
class Board():
    def __init__(self):
        self.ships = []
        self.miss = []
        
    def n_ships(self):
        return len(self.ships)
        
    def live_ships(self):
        live = 0
        for ship in self.ships:
            if ship.life > 0:
                live += 1
        return live
        
    def available(self, ship):
        available_each = []
        for ship_check in self.ships:
            for pos_check in ship.pos:
                available_each.append(pos_check not in ship_check.pos)
        
        return all(available_each)
        
    def add_ship(self, typ, ini_pos, orient):
        ship = Ship(typ, ini_pos, orient)
        inside = True
        for pos in ship.pos:
            if pos[0] > 11 or pos[0] < 0:
                inside = False
            if pos[1] > 11 or pos[1] < 0:
                inside = False
            
        if inside and self.available(ship):
            self.ships.append(ship)
            return 0
        else:
            return 1
            
    def shoot(self, pos):
        hit = 0
        for ship in self.ships:
            hit = hit or ship.shoot(pos)
        
        if not hit:
            self.miss.append(pos)
        
        return hit
        
    def _p(self, i, j, k, hide=False):
        sym = 'water'
        for ship in self.ships:
            if [i,j] in ship.pos:
                sym = 'live'
                typ = ship.typ
                break
            elif [i,j] in ship.pos_hit:
                if ship.life < 1:
                    sym = 'dead'
                    typ = ship.typ
                else:
                    sym = 'hit'
                break
                
        if [i,j] in self.miss and sym=='water':
            sym = 'miss'
        
        if sym == 'live':
            if hide:
                dic = {1:' ', 2:' ', 
                       3:' ', 4:' '}
            else:
                dic = {1:str(typ), 2:str(typ), 
                       3:str(typ), 4:str(typ)}
        elif sym == 'hit':
            dic = {1:'\\', 2:'/', 
                   3:'/' , 4:'\\'}
        elif sym == 'dead':
            dic = {1:str(typ), 2:'/', 
                   3:'/'     , 4:str(typ)}
        elif sym == 'miss':
            dic = {1:'/' , 2:'\\', 
                   3:'\\', 4:'/'}
        else:
            dic = {1:' ', 2:' ', 
                   3:' ', 4:' '}
        
        ch = dic[k]
        
        return ch
        
        
    def print_board(self, hide=False):
        dic = {0:'A', 1:'B', 2:'C', 3:'D',  4:'E',  5:'F',
               6:'G', 7:'H', 8:'I', 9:'J', 10:'K', 11:'L'}
        s = []
        s.append('     01   02   03   04   05   06   07   08   09   10   11   12   \n')
        s.append('   ')
        for i in xrange(12):
            s.append(' ----'*12)
            s.append('  \n')
            s.append(dic[i])
            s.append('  | ')
            for j in xrange(12):
                s.append(self._p(i,j,1,hide=hide))
                s.append(self._p(i,j,2,hide=hide))
                s.append(' | ')
            s.append('\n   | ')
            for j in xrange(12):
                s.append(self._p(i,j,3,hide=hide))
                s.append(self._p(i,j,4,hide=hide))
                s.append(' | ')
            s.append('\n   ')
        s.append(' ----'*12)
        s.append('  ')
        
        return ''.join(s)
    
    
class Game():
    def __init__(self, board1, board2):
        self.board = [board1, board2]
        self.status = 'started'
        self.turn = 0
        self.player_name = ['','']
        
    def play(self, player, pos):
        other = player*-1+1
        
        if self.status == 'started':
        
            if self.turn == player:
                hit = self.board[other].shoot(pos)
                life = self.board[other].live_ships()
                if life == 0:
                    self.status = 'finished'
                if self.status == 'started':
                    self.turn = self.turn*-1+1
                    
                msg = {'my_board':self.board[player].print_board(hide=False),
                       'other_board':self.board[other].print_board(hide=True),
                       'status':self.status,
                       'comm':''}
                if self.status == 'finished':
                    msg['comm'] = 'finished'
                elif hit:
                    msg['comm'] = 'hit'
                else:
                    msg['comm'] = 'miss'
                    
            else:
            
                msg = {'my_board':self.board[player].print_board(hide=False),
                       'other_board':self.board[other].print_board(hide=True),
                       'status':self.status,
                       'comm':'not your turn'}
        
        else:
            msg = {'my_board':self.board[player].print_board(hide=False),
                   'other_board':self.board[other].print_board(hide=True),
                   'status':self.status,
                   'comm':'finished'}
                   
        return msg
        
    def print_full_board(self, player):
        other = player*-1+1
        ret = print_2_col(self.board[player].print_board(hide=False),self.board[other].print_board(hide=True))
        
        return ret
    
    
class Comm:

    def __init__(self, mode, port):
        
        self.mode = mode
        self.port = port
        
        if mode == 'server' or mode == 'host':
        
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.bind(('127.0.0.1', port))
            s.listen(5)
            c, addr = s.accept()
            
            self.sock = c
        
        else:
            
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect(('127.0.0.1', port))
            
            self.sock = s
        
    def send(self, msg):
        self.sock.sendall(pickle.dumps(msg))
    
    def recv(self):
        return pickle.loads(self.sock.recv(10*1024))
    
    def close(self):
        self.sock.close()
    
 
def add_array(a, b):
    c = [0]*len(a)
    for i in xrange(len(a)):
        c[i] = a[i] + b[i]
    return c

    
def print_2_col(s1, s2):
    s1s = s1.split('\n')
    s2s = s2.split('\n')
    
    s = []
    for idx,_ in enumerate(s1s):
        s.append('  ' + s1s[idx] + '    .    ' + s2s[idx] + '  ')
    
    return '\n'.join(s)
    
    
def pos_to_array(pos):
    dic = dict(zip(map(chr, range(ord('a'), ord('z')+1)), range(0, 26)))
    row = dic[pos[0]]
    col = int(pos[1:])-1
    return [row,col]
    
    
def hide_ships(s):
    ship_chars = ['1','2','3','4','5','6','7','8','9']
    s1 = s
    for ch in ship_chars:
        s1 = s1.replace(ch,' ')
    return s1

    
def splash():
    os.system('clear')
    print('')
    print('                      ========  ###   ####  ###        ###    ###   #####  #####  #     ####   ###   #   #  #  ####   ========')
    print('                         =====  #  #  #     #  #       #  #  #   #    #      #    #     #     #      #   #  #  #   #  =====   ')
    print('                            ==  ###   ###   #  #       # #   #####    #      #    #     ###    ###   #####  #  ####   ==      ')
    print('                         =====  #  #  #     #  #       #  #  #   #    #      #    #     #         #  #   #  #  #      =====   ')
    print('                      ========  #  #  ####  ###        ###   #   #    #      #    ####  ####   ###   #   #  #  #      ========')
    print('')
    print('')
    print('                                                   >  THIS GAME MIGHT OR MIGHT NOT CONTAIN  < ')
    print('                                                   >     EXPORT CONTROLLED INFORMATION      < ')
    print('                                  |\            ')
    print('                                  | \           ')
    print('                                  |__\          ')
    print('                           _______|______       ')
    print('                           \            /       ')
    print('                      ~~~~~~\__________/~~~~~~~ ')
    print('')
    
    
def legend():
    s = []
    s.append('Position your export controlled vessels.                                \n')
    s.append('                                                                        \n')
    s.append('Enter the starting position and orientation of each vessel.             \n')
    s.append('Position is given by a letter, corresponding to a row, followed by      \n')
    s.append('a number, corresponding to a column.                                    \n')
    s.append('Orientation is given by v, for vertical, or h, for horizontal.          \n')
    s.append('                                                                        \n')
    s.append('Example: b6v                                                            \n')
    s.append('                                                                        \n')
    s.append('You have 3 export controlled patrol ships, 3 export controlled          \n')
    s.append('destroyers, 2 export controlled battleships and 1 export controlled     \n')
    s.append('aircraft carrier.                                                       \n')
    s.append('                                                                        \n')
    s.append('                                                                        \n')
    s.append('                                                                        \n')
    s.append('                                                                        \n')
    s.append('                                                                        \n')
    s.append('                                                                        \n')
    s.append('                                                                        \n')
    s.append('Legend:                                                                 \n')
    s.append('                                                                        \n')
    s.append('\/                                                                      \n')
    s.append('/\              Hit                                                     \n')
    s.append('                                                                        \n')
    s.append('/\                                                                      \n')
    s.append('\/              Missed                                                  \n')
    s.append('                                                                        \n')
    s.append('22 22           Export controlled                                       \n')
    s.append('22 22           patrol ship                                             \n')
    s.append('                                                                        \n')
    s.append('33 33 33        Export controlled                                       \n')
    s.append('33 33 33        destroyer                                               \n')
    s.append('                                                                        \n')
    s.append('44 44 44 44     Export controlled                                       \n')
    s.append('44 44 44 44     battleship                                              \n')
    s.append('                                                                        \n')
    s.append('55 55 55 55 55  Export controlled                                       \n')
    s.append('55 55 55 55 55  aircraft carrier                                        \n')
    
    return ''.join(s)
    
    
def menu(typ='mode', game=None, player=0):
    other = player*-1+1
    
    if typ == 'mode':
        while True:
            splash()
            print(' Select mode:')
            print('   [1]: Host a session')
            print('   [2]: Connect to session')
            print('')
            print('   [0]: Quit')
            
            inp = raw_input('\o_ ')
            
            if inp == '1':
                mode = 'host'
                menu(typ='host')
            elif inp == '2':
                mode = 'client'
                menu(typ='join')
            elif inp == '0':
                menu(typ='quit')
            else:
                menu(typ='mode')
                
    elif typ == 'host':
        splash()
        print(' Choose a port to host (e.g. 5001). Type 0 to go back.')
        
        inp = raw_input('\o_ ')
        
        if inp == '0':
            menu(typ='mode')
        
        try:
            print(' Waiting for other player...')
            comm = Comm('host', int(inp))
            
        except:
            splash()
            print(' Not possible to host a session.')
            inp = raw_input('\o_ ')
            menu(typ='mode')
        
        play_as_host(comm)
        
    elif typ == 'join':
        splash()
        print(' Choose a port to connect (e.g. 5001). Type 0 to go back.')
        
        inp = raw_input('\o_ ')
        
        if inp == '0':
            menu(typ='mode')
            
        try:
            print(' Waiting for other player...')
            comm = Comm('client', int(inp))
            
        except:
            splash()
            print(' Not possible to connect to this session.')
            inp = raw_input('\o_ ')
            menu(typ='mode')
    
        play_as_client(comm)
    
    elif typ == 'set_board':
        my_board = Board()
        ships = ['export controlled aircraft carrier', 'export controlled battleship', 'export controlled destroyer', 'export controlled patrol ship']
        typ = [5, 4, 3, 2]
        n_ships = [1, 2, 3, 3]
        
        for i_typ in xrange(len(typ)):
            for i_ship in xrange(n_ships[i_typ]):
                ret = 1
                valid = True
                while ret != 0:
                    splash()
                    print('')
                    print(print_2_col(my_board.print_board(), legend()))
                    print('Position an ' + ships[i_typ] + ' (' + str(i_ship+1) + '/' + str(n_ships[i_typ]) + '):')
                    if not valid:
                        print('Try again...')
                    inp = raw_input('\o_ ')
                    try:
                        inp = inp.strip().lower()
                        valid = inp[0] in map(chr, range(ord('a'),ord('l')+1))
                        valid = valid and inp[-1] in ['v','h']
                        valid = valid and int(inp[1:-1]) <= 12 and int(inp[1:-1]) >= 1
                    except:
                        valid = False
                    
                    if valid:
                        ret = my_board.add_ship(typ[i_typ], pos_to_array(inp[:-1]), inp[-1])
                        
        splash()
        print('')
        print(print_2_col(my_board.print_board(), legend()))
        print('Done. Waiting fot the other player...')
        
        return my_board
        
    elif typ == 'play':
        valid = True
        first = True
        while not valid or first:
            first = False
            splash()
            print('                                  YOU                                                                 ENEMY (' + game.player_name[other] + ')')
            print(game.print_full_board(player))
            print('Enter a position to shoot:')
            if not valid:
                print('Try again...')
            inp = raw_input('\o_ ')
            try:
                inp = inp.strip().lower()
                valid = inp[0] in map(chr, range(ord('a'),ord('l')+1))
                valid = valid and int(inp[1:]) <= 12 and int(inp[1:]) >= 1
            except:
                valid = False
        
        msg = game.play(player, pos_to_array(inp))
        
        return msg
        
    elif typ == 'hit':
        
        splash()
        print('                                  YOU                                                                 ENEMY (' + game.player_name[other] + ')')
        print(game.print_full_board(player))
        print('You hit a ship! Waiting for the other player...')
        
        return
        
    elif typ == 'miss':
    
        splash()
        print('                                  YOU                                                                 ENEMY (' + game.player_name[other] + ')')
        print(game.print_full_board(player))
        print('You missed! Waiting for the other player...')
        
        return
    
    elif typ == 'finished':
    
        splash()
        print('                                  YOU                                                                 ENEMY (' + game.player_name[other] + ')')
        print(game.print_full_board(player))
        if game.board[player].live_ships() < 1:
            print('Game finished. You lost!')
        
        else:
            print('Game finished. You won!')
        
        print('Press enter to quit...')
        inp = raw_input('\o_ ')
        
    elif typ == 'quit':
        sys.exit()
    
    
def play_as_host(comm):

    player = 0
    other = 1
    my_name = os.getlogin()
    other_name = None
    finished = False
    
    my_board = menu(typ='set_board')
    
    comm.send(my_name)
    other_name = comm.recv()
    
    other_board = comm.recv()
    game = Game(my_board, other_board)
    game.player_name = [my_name, other_name]
    
    while not finished:
        if game.status != 'finished':
            msg = menu(typ='play', game=game, player=player)
            comm.send(game)
            
            if msg['comm'] == 'hit':
                menu(typ='hit', game=game, player=player)
            elif msg['comm'] == 'miss':
                menu(typ='miss', game=game, player=player)
            elif msg['comm'] == 'finished':
                menu(typ='finished', game=game, player=player)
                finished = True
        else:
            menu(typ='finished', game=game, player=player)
            finished = True
        
        if not finished:
            game = comm.recv()
    
    comm.close()
    menu('quit')

    
def play_as_client(comm):
    
    player = 1
    other = 0
    my_name = os.getlogin()
    other_name = None
    finished = False
    
    my_board = menu(typ='set_board')
    
    comm.send(my_name)
    other_name = comm.recv()
    
    comm.send(my_board)
    
    game = comm.recv()
    
    while not finished:
        if game.status != 'finished':
            msg = menu(typ='play', game=game, player=player)
            comm.send(game)
            
            if msg['comm'] == 'hit':
                menu(typ='hit', game=game, player=player)
            elif msg['comm'] == 'miss':
                menu(typ='miss', game=game, player=player)
            elif msg['comm'] == 'finished':
                menu(typ='finished', game=game, player=player)
                finished = True
        else:
            menu(typ='finished', game=game, player=player)
            finished = True
        
        if not finished:
            game = comm.recv()
    
    comm.close()
    menu('quit')
    
    
if __name__ == '__main__':

    menu()
    
    
