# Red Battleship

Red Battleship is a multiplayer battleship game written in python.

This game was developed to run in an environment consisting of one single computer accessed through several terminals, where different users did not have access to common directories and the only possible way of exchanging information was through TCP/IP localhost.

## Usage

* upload_rbs.py

	This script uploads rbs.py other user. The other user should run download_rbs.py to get the file.

* download_rbs.py

	This script downloads rbs.py. It is a short script that can be typed quickly by any user who wants to get Red Battleship.

* rbs.py

	The main script for the game.

## Related software

FishMarket is another software with the same purpose. A simple chat developed to run in the same environment. https://bitbucket.org/lbergamo/fishmarket/
